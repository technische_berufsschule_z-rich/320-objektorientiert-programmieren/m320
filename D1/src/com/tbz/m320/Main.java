package com.tbz.m320;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Heizung kleineHeizung = new Heizung(0f,0f);

        //hier rufen wir die Heizung auf
        System.out.println("Wie heiss ist es ? ");
        Scanner scan = new Scanner(System.in);
        String Input  = scan.next();
        Float  temp = Float.parseFloat(Input);
        kleineHeizung.setAnfangstemperatur(temp);

        System.out.println("Die Temperatur ist bei "+ Input +" Grad wie viel heisser willst du es machen ? ");
        Input = scan.next();
        Float Inkrement = Float.parseFloat(Input);
        kleineHeizung.setInkrement(Inkrement);

        System.out.println("Die Temperatur ist " +kleineHeizung.addition(kleineHeizung.Anfangstemperatur, kleineHeizung.Inkrement) );
    }
}