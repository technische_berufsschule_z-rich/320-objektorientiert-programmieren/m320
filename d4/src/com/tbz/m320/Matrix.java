package com.tbz.m320;

import java.awt.*;
import java.io.Console;
import java.util.*;
import java.util.List;

public class Matrix {
    Matrix()
    {
        //erstellt Nested Array
        ArrayList<ArrayList<String>> NestedArray = new ArrayList<>();

        //erstellt 7 Listen die als 7 Rows dienen werden
        ArrayList<String> Liste1 = new ArrayList<>();
        ArrayList<String> Liste2 = new ArrayList<>();
        ArrayList<String> Liste3 = new ArrayList<>();
        ArrayList<String> Liste4 = new ArrayList<>();
        ArrayList<String> Liste5 = new ArrayList<>();
        ArrayList<String> Liste6 = new ArrayList<>();
        ArrayList<String> Liste7 = new ArrayList<>();

        //füllt alle 7 Rows erstmals mit Sternen
        FillArray(Liste1);
        FillArray(Liste2);
        FillArray(Liste3);
        FillArray(Liste4);
        FillArray(Liste5);
        FillArray(Liste6);
        FillArray(Liste7);

        //Die 7 Arrays werden der Nested Array die noch leer war geadded
        NestedArray.add(Liste1);
        NestedArray.add(Liste2);
        NestedArray.add(Liste3);
        NestedArray.add(Liste4);
        NestedArray.add(Liste5);
        NestedArray.add(Liste6);
        NestedArray.add(Liste7);

        //die Nested Array wird befüllt mit Buchstaben
        ChangeArray(0,0,NestedArray);

        //die neu befüllte Array wird gezeichnet
        Neuzeichnen(NestedArray);

        //Eine unendliche Schlaufe die dem benutzer Bewegung ermöglicht
        while (true)
        {
            System.out.println();
            System.out.println("Wo willst du hin im Zoo ?");
            System.out.println("Row ?");
            Scanner Scan1 = new Scanner(System.in);
            String Row = Scan1.next();
            System.out.println("Welche Coulmn ?");
            String Column = Scan1.next();
            ChangeArray(Integer.parseInt(Row)-1,Integer.parseInt(Column)-1,NestedArray);
            Neuzeichnen(NestedArray);
        }




        }
        //füllt die einzelnen Rows am Anfang mit Sternen
        public void FillArray(ArrayList<String> Liste)
        {
            for(int i = 0; i < 20 ; i++)
            Liste.add("*");

        }

    //ändert die NestedArray
    public ArrayList<ArrayList<String>> ChangeArray(Integer Row, Integer Column,ArrayList<ArrayList<String>> NestedArray)
    {
        //Tiere im Zoo
        NestedArray.get(5).set(3,"A");
        NestedArray.get(1).set(19,"N");
        NestedArray.get(6).set(16,"G");
        NestedArray.get(0).set(4,"E");
        NestedArray.get(2).set(14,"L");
        NestedArray.get(4).set(17,"P");
        NestedArray.get(6).set(3,"T");

        //schaut ob irgend welche Tiere getroffen wurden
        switch(NestedArray.get(Row).get(Column))
    {
        case "A":
            Menu menu1 = new Menu();
            menu1.Affe();
            break;
        case "N":
            Menu menu2 = new Menu();
            menu2.Nashorn();
            break;
        case "G":
            Menu menu3 = new Menu();
            menu3.Gepard();
            break;
        case "E":
            Menu menu4 = new Menu();
            menu4.Elefant();
            break;
        case "L":
            Menu menu5 = new Menu();
            menu5.Löwe();
            break;
        case "P":
            Menu menu6 = new Menu();
            menu6.Pinguin();
            break;
        case "T":
            Menu menu7 = new Menu();
            menu7.Tiger();
            break;

    }
        //eine Methode die S löscht und durch * ersetzt
        removeS(NestedArray);
        //bewegt Spieler
        NestedArray.get(Row).set(Column,"S");
       return NestedArray;
    }
//Zeichnet die Map neu
    public void Neuzeichnen( ArrayList<ArrayList<String>> NestedArray)
    {

        for (ArrayList<String> s : NestedArray)
        {
            System.out.println();
            for (String p : s)
            {
                System.out.print(p);

            }
        }

    }


     //entfernt immer wieder den Buchstaben S bevor die Map neu gezeichnet wird.
    public void removeS(ArrayList<ArrayList<String>> nestedArray) {
        for (ArrayList<String> row : nestedArray) {
            System.out.println();
            for (int i = 0; i < row.size(); i++) {
                if (row.get(i).equals("S")) {
                    row.set(i, "*");
                }
            }
        }
    }

}


