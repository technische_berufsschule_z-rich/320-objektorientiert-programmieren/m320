package com.tbz.m320;

public class Gepard implements Tiere{
    static int gewicht = 30;
    static double groesse = 1.8;
    static int geschwindigkeit = 70;
    Gepard(){

    }
    @Override
    public void Spezialfaehigkeit() {
        System.out.println("Spezialfähigkeit: Ich kann schnell rennen");
    }

    // Getter and Setter

    public static int getGewicht() {
        return gewicht;
    }

    public static void setGewicht(int gewicht) {
        Gepard.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Gepard.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Gepard.geschwindigkeit = geschwindigkeit;
    }
}
