package com.tbz.m320;

public class Pinguin implements Tiere{
    Pinguin(){};
    static int gewicht = 12;
    static double groesse = 1.2;
    static int geschwindigkeit = 4;
    @Override public void Spezialfaehigkeit()
    {
        System.out.println("Ich kann gut rutschen");
    };

    //Getters and Setters


    public static int getGewicht() {
        return gewicht;
    }

    public static void setGewicht(int gewicht) {
        Pinguin.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Pinguin.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Pinguin.geschwindigkeit = geschwindigkeit;
    }
}
