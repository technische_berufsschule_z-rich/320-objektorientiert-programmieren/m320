package com.tbz.m320;

public class Affe implements Tiere{
    static int gewicht = 12;
    static double groesse = 1.2;
    static int geschwindigkeit = 10;
    Affe(){

    }
    @Override
    public void Spezialfaehigkeit() {
        System.out.println("Spezialfähigkeit: Ich kann klettern");
    }

    //Getter and Setter

    public static int getGewicht() {
        return gewicht;
    }

    public static void setGewicht(int gewicht) {
        Affe.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Affe.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Affe.geschwindigkeit = geschwindigkeit;
    }
}
