package com.tbz.m320;

public class Elefant implements Tiere{
    Elefant(){};
    static int gewicht = 5000;
    static double groesse = 13;

    static int geschwindigkeit = 20;
    @Override
    public void Spezialfaehigkeit()
    {
        System.out.println("ich habe einen langen Rüssel !");
    };


    //Getter and Setter
    public static int getGewicht() {
        return gewicht;
    }

    public static void setGewicht(int gewicht) {
        Elefant.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Elefant.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Elefant.geschwindigkeit = geschwindigkeit;
    }
}
