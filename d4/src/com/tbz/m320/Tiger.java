package com.tbz.m320;

public class Tiger implements Tiere{
    Tiger(){};
    static int gewicht = 120;
    static double groesse = 5.7;
    static int geschwindigkeit = 35;
    @Override public void Spezialfaehigkeit()
    {
        System.out.println("Ich kann mich gut tarnen");
    };

    //Getters and Setters

    public static int getGewicht() {
        return gewicht;
    }
    public static void setGewicht(int gewicht) {
        Tiger.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Tiger.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Tiger.geschwindigkeit = geschwindigkeit;
    }
}
