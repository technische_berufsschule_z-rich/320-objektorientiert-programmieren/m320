package com.tbz.m320;

public class Löwe implements Tiere{
    Löwe(){};
    static int gewicht = 300;
    static double groesse = 5;
    static int geschwindigkeit = 30;
    @Override public void Spezialfaehigkeit()
    {
        System.out.println("Ich kann fest zubeissen");
    };

    //Getter and Setter

    public static int getGewicht() {
        return gewicht;
    }

    public static void setGewicht(int gewicht) {
        Löwe.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Löwe.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Löwe.geschwindigkeit = geschwindigkeit;
    }
}
