package com.tbz.m320;

public class Nashorn implements Tiere{
    static int gewicht = 1200;
    static double groesse = 5.2;
    static int geschwindigkeit = 40;
    Nashorn(){

    }
    @Override
    public void Spezialfaehigkeit() {
        System.out.println("Spezialfähigkeit: Ich kann Sachen durchbohren");
    }





    // Getter und Setter
    public static int getGewicht() {
        return gewicht;
    }

    public static void setGewicht(int gewicht) {
        Nashorn.gewicht = gewicht;
    }

    public static double getGroesse() {
        return groesse;
    }

    public static void setGroesse(double groesse) {
        Nashorn.groesse = groesse;
    }

    public static int getGeschwindigkeit() {
        return geschwindigkeit;
    }

    public static void setGeschwindigkeit(int geschwindigkeit) {
        Nashorn.geschwindigkeit = geschwindigkeit;
    }
}
