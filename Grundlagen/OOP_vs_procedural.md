#### OOP vs. procedural:

**Procedural**:

Prozeduralem Programieren:
 - Fokus: Bezug zu Elementen in der realen Welt
 - Bezug zu Objekten
 - Darstellung des Problems
 - Beschreibung von Verhalten der Objekte

**OOP**:

Objektorientierte Programmierung (OOP):
 - Fokus: Problemlösung mit Prozeduren
 - Prozeduren = Prozeduren sind abgeschlossene, wiederholbare Abfolgen von Anweisungen oder Operationen in der Programmierung, die eine bestimmte Aufgabe oder Funktion ausführen. Sie ermöglichen die Strukturierung von Code und die Wiederverwendbarkeit von Anweisungen.
 - Verwendung von Variablen, Entscheidungen und Schleifen
 - "Ablauf" 


#### Paradigma:

Ein grundlegendes Konzept oder eine Denkweise. Hier haben wir uns gerade zwei verschieden Paradigmas angeschaut: Procedural und OOP. 

