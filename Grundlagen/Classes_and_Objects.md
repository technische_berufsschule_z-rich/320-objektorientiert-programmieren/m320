# Klassen und Objekte:

In OOP wird mit Klassen und Objekte programmiert. Damit können Werte gespeichert und bearbeitet werden.

![Grundgerüst einer Klasse](https://gitlab.com/ch-tbz-it/Stud/m320/-/raw/main/N1-Class_Constructor_Methods/x_gitressourcen/grundger%C3%BCst.JPG)


#### Attribute:
Attribute sind Eigenschaften oder Merkmale (Daten), die einem Objekt oder einer Entität zugeordnet sind.

In diesem Beispiel speichern wir in unserer Klasse:
 - Wer wohnt denn in dem Haus?
 - Wieviele Zimmer hat es?
 - An welcher Strasse ist das Haus gelegen?
 - An welcher Strassennummer?
 - Evtl. der Wert des Hauses

![Klasse ausgebaut](https://gitlab.com/ch-tbz-it/Stud/m320/-/raw/main/N1-Class_Constructor_Methods/x_gitressourcen/bauplan.JPG) 


#### Initialisierung mittels Konstruktor:

Wir geben den Attributen **default** Werte (Anfangswerte). 
Wir setzen diese Anfangswerte im Konstruktor.
Ein Konstruktor setzt die Anfangswerte für die verschiedenen Eigenschaften oder Variablen (Attribute), die dem Objekt (Klasse) zugeordnet sind.

![Klasse ausgebaut mit Konstruktor](https://gitlab.com/ch-tbz-it/Stud/m320/-/raw/main/N1-Class_Constructor_Methods/x_gitressourcen/bauplan_klasse.JPG)

#### Zugriff auf die Attribute via Getter und Setter:

Getter & Setter werden benötigt, um Datenfelder zu lesen oder zu schreiben.
Getter = lesen, Setter = schreiben

```
// getter and setter here
    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
```

#### Testklassen:

In unseren vorherigen Beispielen haben wir die Klasse House.java erstellt. Wir haben nun den Bauplan des Hauses wollen jetzt aber spezifisch ein eigenes Haus bauen (mit Werten wie "Famiie: Müller, Bischof):

![Klasse mit spezifischen Werten](https://gitlab.com/ch-tbz-it/Stud/m320/-/raw/main/N1-Class_Constructor_Methods/x_gitressourcen/aufruf_objekt.JPG)

