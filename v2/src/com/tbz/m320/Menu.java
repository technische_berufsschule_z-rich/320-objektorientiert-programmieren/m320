package com.tbz.m320;

import java.util.Scanner;

public class Menu
{
    // Diese Klasse ermöglicht Spieler zu erstellen un der Klasse hinzuzufügen.
    // Jede Subklasse hat die Methode spielen überschrieben.
    public Menu(){
        Mannschaft Mannschaft1 = new Mannschaft();
        while(true)
        {
        System.out.println("Was willst du ? ");
        System.out.println("A: Angreifer hinzufügen zu Mannschaft");
        System.out.println("B: Verteidiger hinzufügen zu Mannschaft.");
        System.out.println("C: Goalie hinzufügen zu Mannschaft");
        System.out.println("D:Angreifer aus Mannschaft entfernen");
        System.out.println("E: Verteidiger aus Mannschaft entfernen");
        System.out.println("F: Goalie aus Mantschaft entfernen");
        System.out.println("G: Mannschaft anzeigen");
        Scanner scan = new Scanner(System.in);

        String input = scan.nextLine();
        switch(input){
            case "A":
                System.out.println("Wie soll der Angreifer heissen ?");
                input = scan.next();
                Angreifer a = new Angreifer(input);
                Mannschaft1.AddAngreifer(a);
                break;
            case "B":
                System.out.println("Wie soll der Verteidiger heissen ?");
                input = scan.next();
                Verteidiger v = new Verteidiger(input);
                Mannschaft1.AddVerteidiger(v);
                break;
            case "C":
                System.out.println("Wie soll der Goalie heissen ?");
                input = scan.next();
                System.out.println("Wie gross ist der Goealie ?");
                double groesse = Double.parseDouble(scan.next());
                Goalie g = new Goalie(input,groesse);
                Mannschaft1.AddGoalie(g);
                break;
            case "D":
                System.out.println("Welchen Angreifer willst du entfernen ? ");
                input = scan.next();
                Mannschaft1.RemoveAngreifer(input);
                break;
            case "E":
                System.out.println("Welchen Verteidiger willst du entfernen ? ");
                input = scan.next();
                Mannschaft1.RemoveVerteidiger(input);
                break;
            case "F":
                Mannschaft1.RemoveGoalie();
                break;
            case "G":
                Mannschaft1.AlleSpielerAnzeigen();
        }


        }
        /*Angreifer p = new Angreifer("Peter");
        Angreifer v = new Angreifer("Tom");
        Mannschaft1.AddAngreifer(p);
        Mannschaft1.AddAngreifer(v);
        Mannschaft1.RemoveAngreifer("Peter");

        Verteidiger pi = new Verteidiger("Pablo");
        Verteidiger vi = new Verteidiger("sergio");
        Mannschaft1.AddVerteidiger(pi);
        Mannschaft1.AddVerteidiger(vi);
        Mannschaft1.RemoveVerteidiger("sergio");

        Goalie g = new Goalie("Gurt",189.9);
        Mannschaft1.setGoalie(g);
        Mannschaft1.RemoveGoalie();
        Mannschaft1.AlleSpielerAnzeigen();*/
    }

}
