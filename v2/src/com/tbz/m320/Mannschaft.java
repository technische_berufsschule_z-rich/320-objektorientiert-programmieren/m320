package com.tbz.m320;

import java.util.ArrayList;
import java.util.List;

public class Mannschaft {


    private List<Angreifer> angreiferListe;
    private Goalie goalie;
    private List<Verteidiger> verteidigerList;


    public Mannschaft()
    {
        this.angreiferListe = new ArrayList<>();
        this.verteidigerList = new ArrayList<>();


    }

    // Getters and Setters
    public void AddGoalie(Goalie e)
    {
        this.goalie = e;


    }
    public void AddVerteidiger(Verteidiger e)
    {
        verteidigerList.add(e);
    }
    public void AddAngreifer(Angreifer e)
    {
        angreiferListe.add(e);

    }
    public void RemoveAngreifer(String gesuchterName)
    {
        Angreifer baldEntfernt = new Angreifer("Tom");
        for(Angreifer p : angreiferListe){
            if(p.ZeigeName().equals(gesuchterName))
            {
                 baldEntfernt = p;
            }

    }
        angreiferListe.remove(baldEntfernt);
    }
    public void RemoveVerteidiger(String gesuchterName)
    {
        Verteidiger baldEntfernt = new Verteidiger("Tom");
        for(Verteidiger p : verteidigerList){
            if(p.ZeigeName().equals(gesuchterName))
            {
                baldEntfernt = p;
            }

        }
        verteidigerList.remove(baldEntfernt);
    }
    public void RemoveGoalie()
    {
        this.goalie = new Goalie("kein Goalie Grösse:",0);
    }
    public void AlleSpielerAnzeigen(){
        if (angreiferListe.size() != 0)
        {
            for(Angreifer p : angreiferListe)
            {
                System.out.print(p.ZeigeName());
                p.Spielen();
            }
        }
        if(verteidigerList.size() != 0)
        {
            for(Verteidiger p : verteidigerList){
                System.out.print(p.ZeigeName());
                p.Spielen();
            }

        }
        if(this.goalie != null){
            System.out.print(goalie.ZeigeName() + " ");
            goalie.Spielen();
            System.out.println(" Koerpergroesse: "+goalie.KoerperGroesse());
        }


    }
    public List<Angreifer> getAngreiferListe() {
        return angreiferListe;
    }

    public void setAngreiferListe(List<Angreifer> angreiferListe) {
        this.angreiferListe = angreiferListe;
    }

    public Goalie getGoalie() {
        return goalie;
    }

    public void setGoalie(Goalie goalie) {
        this.goalie = goalie;
    }

    public List<Verteidiger> getVerteidigerList() {
        return verteidigerList;
    }

    public void setVerteidigerList(List<Verteidiger> verteidigerList) {
        this.verteidigerList = verteidigerList;
    }


}
