package com.tbz.m320;

public class Goalie extends Spieler {
    private double koerpergroesse = 0;
    public Goalie(String Name, double groesse){
        super(Name);
        this.koerpergroesse = groesse;
    }
    @Override
    public void  Spielen(){
        System.out.print(": Ich bin ein Goalie");

    }

    // Getters and Setters
    public double KoerperGroesse(){
        return koerpergroesse;
    }
    public void setKoerpergroesse(double groesse){
        this.koerpergroesse = groesse;
    }


}
