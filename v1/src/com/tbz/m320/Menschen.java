package com.tbz.m320;

public class Menschen {

    private String name;
    private String haarfarbe;
    private String hautfarbe;

public Menschen(String Name, String Haarfarbe, String Hautfarbe)
{
    this.name = Name;
    this.haarfarbe = Haarfarbe;
    this.hautfarbe = Hautfarbe;
}



// Getter und Setter
public String getName() {
    return name;
}

    public void setName(String name) {
        this.name = name;
    }

    public String getHaarfarbe() {
        return haarfarbe;
    }

    public void setHaarfarbe(String haarfarbe) {
        this.haarfarbe = haarfarbe;
    }

    public String getHautfarbe() {
        return hautfarbe;
    }

    public void setHautfarbe(String hautfarbe) {
        this.hautfarbe = hautfarbe;
    }

}
