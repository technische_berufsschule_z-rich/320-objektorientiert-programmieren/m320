package com.tbz.m320;


public class Schwarze extends Menschen{
    private Integer geschwindigkeit;

    public  Schwarze(Integer Geschwindigkeit ,String Name, String Haarfarbe, String Hautfarbe)
    {
        super(Name,Haarfarbe,Hautfarbe);
        this.geschwindigkeit = Geschwindigkeit;
    }
    public void Vorstellung()
    {
        System.out.println("Name: " + super.getName());
        System.out.println("Haarfarbe: " + super.getHaarfarbe());
        System.out.println("Hautfarbe: " + super.getHautfarbe());
        System.out.println("Geschwindigkeit: " + geschwindigkeit);
    }
}
