package com.tbz.m320;

import javax.naming.Name;

public class Asiaten extends Menschen{

    private Integer iq;
    public Asiaten(Integer IQ,String Name, String Haarfarbe, String Hautfarbe)
    {
        super(Name,Haarfarbe,Hautfarbe);
        this.iq = IQ;

    }
    public void Vorstellung()
    {
        System.out.println("Name: " + super.getName());
        System.out.println("Haarfarbe: " + super.getHaarfarbe());
        System.out.println("Hautfarbe: " + super.getHautfarbe());
        System.out.println("IQ: " + iq);
    }


    //Getter and Setter
    public Integer getIq() {
        return iq;
    }

    public void setIq(Integer iq) {
        this.iq = iq;
    }
}
