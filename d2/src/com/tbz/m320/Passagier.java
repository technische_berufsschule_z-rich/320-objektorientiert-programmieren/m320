package com.tbz.m320;

/**
 * Die Klasse repräsentiert einen Passagier.
 */
public class Passagier {

    /** Der Name des Passagiers. */
    private String name;

    /**
     * Konstruktor für die Passagierklasse.
     *
     * @param name Der Name des Passagiers.
     */
    public Passagier(String name) {
        this.name = name;
    }

    /**
     * Gibt den Namen des Passagiers aus.
     */
    public void nameAusgeben() {
        System.out.println(name);
    }

    /**
     * Getter-Methode für den Namen des Passagiers.
     *
     * @return Der Name des Passagiers.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter-Methode für den Namen des Passagiers.
     *
     * @param name Der neue Name des Passagiers.
     */
    public void setName(String name) {
        this.name = name;
    }
}
