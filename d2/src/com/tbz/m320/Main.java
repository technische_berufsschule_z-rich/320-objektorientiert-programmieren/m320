package com.tbz.m320;

import java.util.Scanner;

/**
 * Die Hauptklasse für das Flugverwaltungssystem.
 */
public class Main {

    /**
     * Die Hauptmethode, die das Programm startet.
     *
     * @param args Die Eingabeparameter für das Programm (nicht verwendet).
     */
    public static void main(String[] args) {
        // Erstellen eines Flugobjekts
        Flug flug1 = new Flug();
        String input;

        // Start der Benutzereingabeschleife
        do {
            // Anzeige des Hauptmenüs
            System.out.println("Hallo, was möchten Sie tun?");
            System.out.println("A: Passagier eintragen.");
            System.out.println("B: Passagier entfernen.");
            System.out.println("C: Passagiere im Flug anzeigen.");
            System.out.println("beenden: Programm beenden");

            // Benutzereingabe verarbeiten
            Scanner scanner = new Scanner(System.in);
            input = scanner.nextLine();

            // Fallunterscheidung basierend auf Benutzereingabe
            if (input.equals("A")) {
                // Passagier hinzufügen
                System.out.println("Wie heißt der neue Passagier?");
                input = scanner.next();
                Passagier passagier = new Passagier(input);
                flug1.Passagiere.add(passagier);
            } else if (input.equals("B")) {
                // Passagier entfernen
                System.out.println("Welchen Passagier möchten Sie entfernen?");
                input = scanner.next();
                for (int i = 0; i < flug1.Passagiere.size(); i++) {
                    if (flug1.Passagiere.get(i).getName().equals(input)) {
                        flug1.Passagiere.remove(i);
                    }
                }
            } else if (input.equals("C")) {
                // Passagierliste ausgeben
                flug1.PassagierlisteAusgeben();
            }
        } while (!input.equals("beenden"));
    }
}
