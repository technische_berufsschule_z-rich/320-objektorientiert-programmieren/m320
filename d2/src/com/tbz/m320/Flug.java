package com.tbz.m320;

import java.util.ArrayList;
import java.util.List;

/**
 * Die Klasse repräsentiert einen Flug und verwaltet die Liste der Passagiere.
 */
public class Flug {

    /** Liste der Passagiere auf dem Flug. */
    List<Passagier> Passagiere;

    /**
     * Konstruktor für die Flugklasse.
     * Initialisiert die Liste der Passagiere als leere ArrayList.
     */
    Flug() {
        this.Passagiere = new ArrayList<>();
    }

    /**
     * Gibt die Liste der Passagiere auf dem Flug aus.
     */
    void PassagierlisteAusgeben() {
        for (Passagier passagier : Passagiere) {
            passagier.nameAusgeben();
        }
    }
}
