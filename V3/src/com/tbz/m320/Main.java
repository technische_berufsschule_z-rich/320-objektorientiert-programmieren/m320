package com.tbz.m320;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Initialisierung Aller Märkte hier schon weil es sonst Fehler gibt
        Swissstockmarket schweiz = new Swissstockmarket();
        EUstockmarket europa = new EUstockmarket();
        USstockmarket USA = new USstockmarket();

        System.out.println("Welchen Markt willst du ?");
        System.out.println("A: Schweizer Markt");
        System.out.println("B: Europamarkt");
        System.out.println("C: US Markt");
        Scanner Scan1 = new Scanner(System.in);
        String Input = Scan1.next();
        switch (Input){
            case "A":

                System.out.println("Schweizer Aktien: ");
                System.out.println("Welche Aktien hast du ? ");
                schweiz.AlleAktienMarketAnzeigen();
                Scan1 = new Scanner(System.in); // Scanner zurücksetzen
                Input = Scan1.nextLine();
                List<String> Aktienliste = StringZuList(Input);
                Portfolio portfolio1 = new Portfolio(Aktienliste,schweiz);
                portfolio1.ShowPortfolio();
                System.out.println("Dein Portfolio hat den Value : "+
                        portfolio1.CalculatePortfolioValue()+" CHF");


                break;
            case "B":

                System.out.println("Europaeische Aktien: ");
                System.out.println("Welche Aktien hast du ? ");
                europa.AlleAktienMarketAnzeigen();
                Scan1 = new Scanner(System.in); // Scanner zurücksetzen
                Input = Scan1.nextLine();
                List<String> Aktienliste2 = StringZuList(Input);
                Portfolio portfolio2 = new Portfolio(Aktienliste2,europa);
                portfolio2.ShowPortfolio();
                System.out.println("Dein Portfolio hat den Value : "+
                        portfolio2.CalculatePortfolioValue() + " Euro");
                break;
            case "C":
                System.out.println("US Aktien: ");
                System.out.println("Welche Aktien hast du ? ");
                USA.AlleAktienMarketAnzeigen();
                Scan1 = new Scanner(System.in); // Scanner zurücksetzen
                Input = Scan1.nextLine();
                List<String> Aktienliste3 = StringZuList(Input);
                Portfolio portfolio3 = new Portfolio(Aktienliste3,USA);
                portfolio3.ShowPortfolio();
                System.out.println("Dein Portfolio hat den Value : "+
                        portfolio3.CalculatePortfolioValue() + " Dollar");
                break;
            default:
                System.out.println("Falsche Eingabe ! bitte gebe A,B oder C ein!");
                break;
        }
    }
    public static List<String> StringZuList(String Eingabe){
        String[] Teile = Eingabe.split("\\s+");
        List<String> Ausgabe = new ArrayList<>();
        for (String p : Teile)
        {
            Ausgabe.add(p);
        }
        return Ausgabe;
    }

}

