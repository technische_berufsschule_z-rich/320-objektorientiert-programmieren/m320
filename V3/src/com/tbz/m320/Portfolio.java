package com.tbz.m320;

import java.util.List;

public class Portfolio {
    List<String> Aktienliste;
    stockExchange stockExchange;
    Portfolio(List<String> Aktienliste, stockExchange stockExchange){
        this.stockExchange = stockExchange;
        this.Aktienliste = Aktienliste;
    }
    double CalculatePortfolioValue(){
     double totalValue = 0;
        for (String StockInPortfolio: Aktienliste ){
            totalValue += stockExchange.GetPrice(StockInPortfolio);
        }
        return totalValue;
    }
    void ShowPortfolio(){
        System.out.println("Dein Portfolio:");
        for (String p : Aktienliste){
            System.out.println(stockExchange.GetName(p)+" "+stockExchange.GetPrice(p)
            +" "+stockExchange.GetWaehrung(p));
        }


    }

}
