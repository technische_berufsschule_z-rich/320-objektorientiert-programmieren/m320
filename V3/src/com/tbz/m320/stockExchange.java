package com.tbz.m320;

public interface stockExchange {
    double GetPrice(String Stockname);
    void AlleAktienMarketAnzeigen();
    String GetName(String Stockname);
    String GetWaehrung(String Stockname);

}
