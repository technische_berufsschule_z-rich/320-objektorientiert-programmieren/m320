package com.tbz.m320;

public class Stock {
    String name;
    double price;
    String waehrung;
    Stock(String Name, double Price,String Waehrung){
        this.name = Name;
        this.price = Price;
        this.waehrung = Waehrung;

    }

    // Getter and Setter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getWaehrung() {
        return waehrung;
    }

    public void setWaehrung(String waehrung) {
        this.waehrung = waehrung;
    }

}
