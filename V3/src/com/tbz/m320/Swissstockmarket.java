package com.tbz.m320;

import java.util.ArrayList;
import java.util.List;

public class Swissstockmarket implements stockExchange {
    List<Stock> Stockliste = new ArrayList<>();
    Swissstockmarket(){
        Stock Apple = new Stock("Apple", 370, "chf");
        Stock Microsoft = new Stock("Microsoft",110,"chf");
        Stock Google = new Stock("Google", 330,"chf");
        Stock Intel = new Stock("Intel", 240, "chf");
        Stockliste.add(Apple);
        Stockliste.add(Microsoft);
        Stockliste.add(Google);
        Stockliste.add(Intel);
    };
    @Override
    public double GetPrice(String Stockname) {
        double GesuchteZahl = 0;
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchteZahl = p.getPrice();
            }
        }
        return GesuchteZahl;
    }
    @Override
    public String GetName(String Stockname) {
        String GesuchterName = "";
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchterName = p.getName();
            }
        }
        return GesuchterName;
    }
    @Override
    public String GetWaehrung(String Stockname) {
        String GesuchteWaehrung = "";
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchteWaehrung = p.getWaehrung();
            }
        }
        return GesuchteWaehrung;
    }
    @Override
    public void AlleAktienMarketAnzeigen(){
        for(Stock p : Stockliste){
            System.out.println(p.name+" "+p.price+" "+p.waehrung);
        }
    }



}
