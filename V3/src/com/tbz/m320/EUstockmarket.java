package com.tbz.m320;

import java.util.ArrayList;
import java.util.List;

public class EUstockmarket implements stockExchange{
    List<Stock> Stockliste = new ArrayList<>();
    EUstockmarket(){
        Stock Apple = new Stock("Apple",180,"EUR");
        Stock Google = new Stock("Google",190,"EUR");
        Stock Intel = new Stock("Intel",200,"EUR");
        Stock Microsoft = new Stock("Microsoft",190,"EUR");
        Stockliste.add(Apple);
        Stockliste.add(Google);
        Stockliste.add(Intel);
        Stockliste.add(Microsoft);
    }
    @Override
    public double GetPrice(String Stockname){
        double Price = 0;
        for(Stock p : Stockliste)
        {
            if(p.getName().equals(Stockname)){
                Price = p.getPrice();
            }
        }
        return Price;
    }
    @Override
    public String GetName(String Stockname) {
        String GesuchterName = "";
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchterName = p.getName();
            }
        }
        return GesuchterName;
    }
    @Override
    public String GetWaehrung(String Stockname) {
        String GesuchteWaehrung = "";
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchteWaehrung = p.getWaehrung();
            }
        }
        return GesuchteWaehrung;
    }
    @Override
    public void AlleAktienMarketAnzeigen(){
        for(Stock p : Stockliste){
            System.out.println(p.name+" "+p.price+" "+p.waehrung);
        }
    }

}
