package com.tbz.m320;

import java.util.ArrayList;
import java.util.List;

public class USstockmarket implements stockExchange {
    List<Stock> Stockliste = new ArrayList<>();
    USstockmarket(){
        Stock Apple = new Stock("Apple", 170, "usd");
        Stock Microsoft = new Stock("Microsoft",210,"usd");
        Stock Google = new Stock("Google", 300,"usd");
        Stock Intel = new Stock("Intel", 260, "usd");
        Stockliste.add(Apple);
        Stockliste.add(Microsoft);
        Stockliste.add(Google);
        Stockliste.add(Intel);
    };
    @Override
    public double GetPrice(String Stockname) {
        double GesuchteZahl = 0;
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchteZahl = p.getPrice();
            }
        }
        return GesuchteZahl;
    }
    @Override
    public String GetName(String Stockname) {
        String GesuchterName = "";
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchterName = p.getName();
            }
        }
        return GesuchterName;
    }
    @Override
    public String GetWaehrung(String Stockname) {
        String GesuchteWaehrung = "";
        for(Stock p : Stockliste){
            if(p.getName().equals(Stockname)){
                GesuchteWaehrung = p.getWaehrung();
            }
        }
        return GesuchteWaehrung;
    }
    @Override
    public void AlleAktienMarketAnzeigen(){
        for(Stock p : Stockliste){
            System.out.println(p.name+" "+p.price+" "+p.waehrung);
        }
    }
}
